# Atlassian Coding Exercise

[Live Demo](https://atlantis-rf.herokuapp.com)

![Atlantis](./atlantis.png)


## Build Steps
#### To install dependencies:
```javascript
npm install
```
#### To run locally:
```javascript
npm start
```

#### To run tests:
```javascript
npm test
```
