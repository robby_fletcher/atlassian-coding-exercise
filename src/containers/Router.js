// Package Imports
import Proptypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';
import { Typography } from '@material-ui/core';

// Local File Imports
import { fetchSpaces } from '../actions';
import App from './App';
import Nav from './Nav';

class RouterWrapper extends React.Component {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;
    dispatch(fetchSpaces());
  }

  render() {
    const { currentSpace, spaces } = this.props;
    const space = spaces.find(s => (currentSpace === s.sys.id));

    // If there is no url param, redirect to the
    // first space returned from the spaces API.
    // Else, render the App
    return (
      <Router>
        <Nav>
          <React.Fragment>
            <Route
              exact
              path="/"
              render={() => {
                if (space !== undefined) {
                  return (<Redirect push to={`/${space.sys.id}`} />);
                }
                return (
                  <Typography variant="h2" gutterBottom>
                    Loading..
                  </Typography>
                );
              }}
            />
            <Route path="/:spaceId" component={App} />
          </React.Fragment>
        </Nav>
      </Router>
    );
  }
}

RouterWrapper.propTypes = {
  currentSpace: Proptypes.string.isRequired,
  dispatch: Proptypes.func.isRequired,
  spaces: Proptypes.arrayOf(Proptypes.object).isRequired,
};

export default connect(state => (state))(RouterWrapper);
