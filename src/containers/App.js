// Package Imports
import Proptypes from 'prop-types';
import { Card, CardContent, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { connect } from 'react-redux';

// Local File Imports
import { changeCurrentSpace } from '../actions';
import TabsWrapper from './Tabs';

// CSS Styling
const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: 240,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 240,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  toolbar: theme.mixins.toolbar,
});

class App extends React.Component {
  constructor(props) {
    super(props);
    const { dispatch, match } = this.props;
    // Get spaceId from the URL, and update state accordingly
    dispatch(changeCurrentSpace(match.params.spaceId));
  }

  render() {
    const { currentSpace, spaces } = this.props;
    const space = spaces.find(s => (currentSpace === s.sys.id));

    if (space !== undefined) {
      return (
        <React.Fragment>
          <Typography variant="h2" gutterBottom>
            {space.fields.title}
          </Typography>
          <Typography variant="h6" gutterBottom>
            {`Created By ${space.createdBy.fields.name}`}
          </Typography>
          <Card>
            <CardContent>
              <Typography>
                {space.fields.description}
              </Typography>
            </CardContent>
          </Card>
          <br />
          <TabsWrapper />
        </React.Fragment>
      );
    }
    return (
      <Typography variant="h2" gutterBottom>
        Loading..
      </Typography>
    );
  }
}

App.propTypes = {
  currentSpace: Proptypes.string.isRequired,
  dispatch: Proptypes.func.isRequired,
  match: Proptypes.objectOf(Proptypes.any).isRequired,
  spaces: Proptypes.arrayOf(Proptypes.object).isRequired,
};

export default connect(state => (state))(withStyles(styles)(App));
