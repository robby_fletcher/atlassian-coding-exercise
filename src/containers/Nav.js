// Package Imports
import Proptypes from 'prop-types';
import {
  AppBar, Drawer, CssBaseline, List, ListItem, ListItemText, Toolbar, Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';

// Local File Imports
import { changeCurrentSpace, fetchAssets, fetchEntries } from '../actions';

// CSS Styling
const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: 240,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 240,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  toolbar: theme.mixins.toolbar,
  routerLinks: {
    textDecoration: 'none',
  },
});

class Nav extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(spaceID) {
    const { dispatch } = this.props;
    // Navigate to a different Space, and load resources for that space.
    dispatch(changeCurrentSpace(spaceID));
    dispatch(fetchAssets(spaceID));
    dispatch(fetchEntries(spaceID));
  }

  render() {
    const {
      children,
      currentSpace,
      classes,
      spaces,
    } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" color="inherit" noWrap>
              Atlantis
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.toolbar} />
          <List>
            {spaces.map(space => (
              <Link key={space.sys.id} className={classes.routerLinks} to={`/${space.sys.id}`}>
                <ListItem
                  button
                  selected={currentSpace === space.sys.id}
                  onClick={() => (this.handleClick(space.sys.id))}
                >
                  <ListItemText primary={space.fields.title} />
                </ListItem>
              </Link>
            ))}
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          { children }
        </main>
      </div>
    );
  }
}

Nav.propTypes = {
  children: Proptypes.element.isRequired,
  currentSpace: Proptypes.string.isRequired,
  classes: Proptypes.objectOf(Proptypes.any).isRequired,
  dispatch: Proptypes.func.isRequired,
  spaces: Proptypes.arrayOf(Proptypes.object).isRequired,
};

export default withRouter(connect(state => (state))(withStyles(styles)(Nav)));
