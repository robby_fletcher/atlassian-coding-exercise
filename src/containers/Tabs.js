// Package Imports
import Proptypes from 'prop-types';
import { Tab, Tabs } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { connect } from 'react-redux';

// Local File Imports
import { changeTab } from '../actions';
import TableWrapper from './Table';

// CSS Styling
const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.background.paper,
  },
});

class TabsWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, tab) {
    const { dispatch } = this.props;
    // Tab clicked, change state accordingly.
    dispatch(changeTab(tab));
  }

  render() {
    const { classes, currentTab } = this.props;

    return (
      <React.Fragment>
        <div className={classes.root}>
          <Tabs value={currentTab} onChange={this.handleChange}>
            <Tab value="entries" label="Entries" />
            <Tab value="assets" label="Assets" />
          </Tabs>
        </div>
        <TableWrapper />
      </React.Fragment>
    );
  }
}

TabsWrapper.propTypes = {
  classes: Proptypes.objectOf(Proptypes.any).isRequired,
  currentTab: Proptypes.string.isRequired,
  dispatch: Proptypes.func.isRequired,
};

const mapStateToProps = state => (state);

export default connect(mapStateToProps)(withStyles(styles)(TabsWrapper));
