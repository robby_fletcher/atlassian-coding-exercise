// Package Imports
import Proptypes from 'prop-types';
import {
  Card, CardContent, Paper, Table, TableBody, TableCell,
  TableHead, TableRow, TableSortLabel, Tooltip, Typography,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { connect } from 'react-redux';

// Local File Imports
import { changeSort, fetchAssets, fetchEntries } from '../actions';

// CSS Styling
const styles = () => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
});

class TableWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.handleHeaderClick = this.handleHeaderClick.bind(this);
  }

  componentWillMount() {
    const { currentSpace, dispatch } = this.props;
    // Get the data for the tables.
    dispatch(fetchAssets(currentSpace));
    dispatch(fetchEntries(currentSpace));
  }

  handleHeaderClick(table, header) {
    const { dispatch } = this.props;
    // Header clicked, change sorting accordingly.
    dispatch(changeSort(table, header));
  }

  render() {
    const {
      assets,
      classes,
      currentTab,
      entries,
    } = this.props;
    const table = currentTab === 'entries' ? entries : assets;

    if (table.data.length === 0) {
      return (
        <Card>
          <CardContent>
            <Typography>
              {`No ${currentTab} found.`}
            </Typography>
          </CardContent>
        </Card>
      );
    }

    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                {table.columns.map(col => (
                  <TableCell
                    key={col.field}
                    numeric={false}
                    padding="default"
                    sortDirection={table.sortDir}
                  >
                    <Tooltip
                      title="Sort"
                      placement="bottom-end"
                      enterDelay={300}
                    >
                      <TableSortLabel
                        active={col.field === table.sortBy}
                        direction={table.sortDir}
                        onClick={() => (this.handleHeaderClick(currentTab, col.field))}
                      >
                        {col.display}
                      </TableSortLabel>
                    </Tooltip>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {table.data.map(row => (
                <TableRow hover key={row.sys.id}>
                  {table.columns.map(col => (
                    <TableCell key={col.field}>{row.fields[col.field]}</TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </div>
      </Paper>
    );
  }
}

TableWrapper.propTypes = {
  assets: Proptypes.objectOf(Proptypes.any).isRequired,
  classes: Proptypes.objectOf(Proptypes.any).isRequired,
  currentSpace: Proptypes.string.isRequired,
  currentTab: Proptypes.string.isRequired,
  dispatch: Proptypes.func.isRequired,
  entries: Proptypes.objectOf(Proptypes.any).isRequired,
};

export default connect(state => (state))(withStyles(styles)(TableWrapper));
