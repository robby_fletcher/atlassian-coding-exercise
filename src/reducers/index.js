import defaultState from './defaultState';

const rootReducer = (state = defaultState, action) => {
  switch (action.type) {
    // Update currentSpace field in state
    case 'CHANGE_CURRENT_SPACE':
      return {
        ...state,
        currentSpace: action.spaceID,
      };
    // Change the sort column, or change sort direction, depending on what was clicked
    case 'CHANGE_SORT': {
      let { sortDir } = state[action.table];
      if (state[action.table].sortBy === action.header) {
        sortDir = (state[action.table].sortDir === 'desc' ? 'asc' : 'desc');
      } else {
        sortDir = 'desc';
      }
      return {
        ...state,
        [action.table]: {
          ...state[action.table],
          data: state[action.table].data.sort((a, b) => {
            if (a.fields[action.header] < b.fields[action.header]) {
              return sortDir === 'desc' ? -1 : 1;
            }
            if (a.fields[action.header] > b.fields[action.header]) {
              return sortDir === 'desc' ? 1 : -1;
            }
            return 0;
          }),
          sortBy: action.header,
          sortDir,
        },
      };
    }
    // Change the current tab (either Entries or Assets)
    case 'CHANGE_TAB':
      return {
        ...state,
        currentTab: action.tab,
      };
    // API Call to get Assets was successful; update state accordingly.
    case 'FETCH_ASSETS_SUCCESS':
      if (action.assets.error) {
        return {
          ...state,
          assets: {
            ...state.assets,
            data: [],
          },
        };
      }
      // Transform API response to map to our state
      return {
        ...state,
        assets: {
          ...state.assets,
          data: action.assets.map(asset => ({
            ...asset,
            fields: {
              ...asset.fields,
              createdBy: state.users
                .find(user => (asset.sys.createdBy === user.sys.id)).fields.name,
              updatedBy: state.users
                .find(user => (asset.sys.updatedBy === user.sys.id)).fields.name,
              updatedAt: new Date(asset.sys.updatedAt).toLocaleDateString('en-US',
                {
                  month: 'numeric',
                  day: '2-digit',
                  year: 'numeric',
                }),
            },
          })),
        },
      };
    // API Call to get Entries was successful; update state accordingly.
    case 'FETCH_ENTRIES_SUCCESS':
      if (action.entries.error) {
        return {
          ...state,
          entries: {
            ...state.entries,
            data: [],
          },
        };
      }
      // Transform API response to map to our state
      return {
        ...state,
        entries: {
          ...state.entries,
          data: action.entries.map(entry => ({
            ...entry,
            fields: {
              ...entry.fields,
              createdBy: state.users
                .find(user => (entry.sys.createdBy === user.sys.id)).fields.name,
              updatedBy: state.users
                .find(user => (entry.sys.updatedBy === user.sys.id)).fields.name,
              updatedAt: new Date(entry.sys.updatedAt).toLocaleDateString('en-US',
                {
                  month: 'numeric',
                  day: '2-digit',
                  year: 'numeric',
                }),
            },
          })),
        },
      };
    // API Call to get Spaces was successful; update state accordingly.
    case 'FETCH_SPACES_SUCCESS':
      // Transform API response to map to our state
      return ({
        ...state,
        // If there is no currentSpace already set (0), set it to
        // the first space in the response.
        currentSpace: state.currentSpace === '0'
          ? action.spaces.items[0].sys.id
          : state.currentSpace,
        spaces: action.spaces.items.map(space => ({
          ...space,
          createdBy: state.users.find(user => (space.sys.createdBy === user.sys.id)),
        })),
      });
    // API Call to get Users was successful; update state accordingly.
    case 'FETCH_USERS_SUCCESS':
      // Transform API response to map to our state
      return {
        ...state,
        spaces: state.spaces.map(space => ({
          ...space,
          createdBy: action.users.find(user => (space.sys.createdBy === user.sys.id)),
        })),
        users: action.users.items,
      };
    default:
      return state;
  }
};

export default rootReducer;
