export function changeCurrentSpace(spaceID) {
  return {
    type: 'CHANGE_CURRENT_SPACE',
    spaceID,
  };
}

export function changeSort(table, header) {
  return {
    type: 'CHANGE_SORT',
    table,
    header,
  };
}

export function changeTab(tab) {
  return {
    type: 'CHANGE_TAB',
    tab,
  };
}

function fetchAssetsSuccess(assets) {
  return {
    type: 'FETCH_ASSETS_SUCCESS',
    assets,
  };
}

function fetchEntriesSuccess(entries) {
  return {
    type: 'FETCH_ENTRIES_SUCCESS',
    entries,
  };
}

function fetchSpacesSuccess(spaces) {
  return {
    type: 'FETCH_SPACES_SUCCESS',
    spaces,
  };
}

function fetchUsersSuccess(users) {
  return {
    type: 'FETCH_USERS_SUCCESS',
    users,
  };
}

// Resuable func to call an API
function fetchData(url, cb) {
  fetch(url)
    .then((res) => {
      if (!res.ok) {
        cb(false);
        throw Error(res.statusText);
      }
      return res;
    })
    .then(res => (res.json()))
    .then(data => cb(data));
}

// Hit both the spaces and users API.
// The users API is being hit here so that we can map user's to their spaces.
export function fetchSpaces() {
  return (dispatch) => {
    fetchData('/users/', data => (dispatch(fetchUsersSuccess(data))));
    fetchData('/space/', data => (dispatch(fetchSpacesSuccess(data))));
  };
}

// Hit the assets API.
export function fetchAssets(space) {
  return dispatch => (
    fetchData(`/space/${space}/assets`, data => (dispatch(fetchAssetsSuccess(data))))
  );
}

// Hit the entries API.
export function fetchEntries(space) {
  return dispatch => (
    fetchData(`/space/${space}/entries`, data => (dispatch(fetchEntriesSuccess(data))))
  );
}
