const express = require('express');
const morgan = require('morgan');
const path = require('path');

const assets = require('./assets.json');
const entries = require('./entries.json');
const spaces = require('./spaces.json');
const users = require('./users.json');

const app = express();
const port = process.env.PORT || 3000;

app.use(morgan('combined'));

app.get('/space', (req, res) => {
  res.json(spaces);
});

app.get('/space/:spaceId', (req, res) => {
  const space = spaces.items.filter((e) => (e.sys.id == req.params.spaceId));

  if (space.length) {
    res.json(space);
  } else {
    res.json({"error": "space not found"});
  }
});

app.get('/space/:spaceId/entries', (req, res) => {
  const entry = entries.items.filter((e) => (e.sys.space == req.params.spaceId));

  if (entry.length) {
    res.json(entry);
  } else {
    res.json({"error": "entries not found"});
  }
});

app.get('/space/:spaceId/entries/:entryId', (req, res) => {
  const entry = entries.items.filter((e) => (e.sys.space == req.params.spaceId && e.sys.id == req.params.entryId));

  if (entry.length) {
    res.json(entry);
  } else {
    res.json({"error": "entry not found"});
  }
});

app.get('/space/:spaceId/assets', (req, res) => {
  const asset = assets.items.filter((e) => (e.sys.space == req.params.spaceId));

  if (asset.length) {
    res.json(asset);
  } else {
    res.json({"error": "assets not found"});
  }
});

app.get('/space/:spaceId/assets/:assetId', (req, res) => {
  const asset = assets.items.filter((e) => (e.sys.space == req.params.spaceId && e.sys.id == req.params.assetId));

  if (asset.length) {
    res.json(asset);
  } else {
    res.json({"error": "asset not found"});
  }
});

app.get('/users', (req, res) => {
  res.json(users);
});

app.get('/users/:userId', (req, res) => {
  const user = users.items.filter((e) => (e.sys.id == req.params.userId));

  if (user.length) {
    res.json(user);
  } else {
    res.json({"error": "user not found"});
  }
});

app.use(express.static(path.resolve(__dirname, 'dist')));

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname + '/dist', 'index.html'));
});

app.listen(port, () => (console.log(`Backend running on port ${port}.`)));
