import * as actions from '../src/actions';
import defaultState from '../src/reducers/defaultState';

describe('actions', () => {
  it('should create action to change current space', () => {
    const spaceID = 'abc123';
    const expected = {
      type: 'CHANGE_CURRENT_SPACE',
      spaceID,
    }
    expect(actions.changeCurrentSpace(spaceID)).toEqual(expected);
  });

  it('should create action to change sort', () => {
    const table = 'entries';
    const header = 'updatedBy';
    const expected = {
      type: 'CHANGE_SORT',
      table,
      header,
    }
    expect(actions.changeSort(table, header)).toEqual(expected);
  });

  it('should create action to change tab', () => {
    const tab = 'assets';
    const expected = {
      type: 'CHANGE_TAB',
      tab,
    }
    expect(actions.changeTab(tab)).toEqual(expected);
  });
});
